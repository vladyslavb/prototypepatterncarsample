//
//  Car.h
//  PrototypePatternCarSample
//
//  Created by Vladyslav Bedro on 10/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Car : NSObject <NSCopying>

//property
@property (strong, nonatomic) NSString*  model;
@property (strong, nonatomic) NSString*  engine;
@property (strong, nonatomic) NSString*  autoPilotVersion;
@property (assign, nonatomic) NSUInteger wheelsSize;
@property (assign, nonatomic) NSUInteger speed;

//methods
- (id) copyWithZone: (nullable NSZone*) zone;

@end

NS_ASSUME_NONNULL_END
