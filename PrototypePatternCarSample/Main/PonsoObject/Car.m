//
//  Car.m
//  PrototypePatternCarSample
//
//  Created by Vladyslav Bedro on 10/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "Car.h"

@implementation Car

- (id) copyWithZone: (NSZone*) zone
{
    Car* copyCar = [[self class] allocWithZone: zone];
    
    copyCar.model            = self.model;
    copyCar.engine           = self.engine;
    copyCar.autoPilotVersion = self.autoPilotVersion;
    copyCar.wheelsSize       = self.wheelsSize;
    copyCar.speed            = self.speed;
    
    return copyCar;
}

@end
