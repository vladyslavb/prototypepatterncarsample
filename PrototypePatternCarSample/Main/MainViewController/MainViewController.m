//
//  MainViewController.m
//  PrototypePatternCarSample
//
//  Created by Vladyslav Bedro on 10/29/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "MainViewController.h"

//classes
#import "Car.h"

@interface MainViewController ()

//properties
@property (strong, nonatomic) Car* firstCar;
@property (strong, nonatomic) Car* secondCar;

//outlets
@property (weak, nonatomic) IBOutlet UILabel*     firstCarModelLabel;
@property (weak, nonatomic) IBOutlet UILabel*     secondCarModelLabel;
@property (weak, nonatomic) IBOutlet UITextField* secondCarModelField;


@end

@implementation MainViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self configureController];
}


#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Internal methods -

- (void) configureController
{
    [self createFirstCar];
    [self createSecondCar];
}

- (void) createFirstCar
{
    self.firstCar = [[Car alloc] init];
    
    self.firstCar.model            = @"Tesla Model S";
    self.firstCar.engine           = @"V8";
    self.firstCar.autoPilotVersion = @"ap_1.0";
    self.firstCar.wheelsSize       = 20;
    self.firstCar.speed            = 320;
}

- (void) createSecondCar
{
    self.secondCar = self.firstCar.copy;
}


#pragma mark - Actions -

- (IBAction) onSetterButtonPressed: (UIButton*) sender
{
    if(self.secondCarModelField.text.length > 0)
        self.secondCar.model = self.secondCarModelField.text;
    
    self.secondCarModelLabel.text = self.secondCar.model;
    
    self.firstCarModelLabel.text = self.firstCar.model;
}


@end
